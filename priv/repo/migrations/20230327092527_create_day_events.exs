defmodule Calchemy.Repo.Migrations.CreateDayEvents do
  use Ecto.Migration

  def change do
    create table(:day_events, primary_key: false) do
      add :id, :uuid, primary_key: true, null: false
      add :calendar_id, references(:calendars, on_delete: :delete_all, type: :uuid), null: false
      add :date, :date
      add :text, :text

      timestamps()
    end
  end
end
