export default function create_hooks() {
  return {
    TextareaFocusHook,
    ScrollToMainDate,
    ToggleOneYearView
  }
}

const TextareaFocusHook = {
  mounted() {
    this.setHeight()
    this.el.focus()
    this.el.addEventListener('input', () => this.setHeight())
  },
  setHeight() {
    this.el.style.height = Math.max(this.el.scrollHeight, this.el.clientHeight) + 'px'
  }
}

const ScrollToMainDate = {
  mounted() {
    this.el.scrollIntoView()
  }
}

const ToggleOneYearView = {
  mounted() {
    let shown = false;
    this.el.addEventListener('click', () => {
      el_classes = document.getElementById('one-year-view').classList
      shown = !shown
      if (shown) {
        el_classes.remove("hidden")
      } else {
        el_classes.add("hidden")
      }
    })
  }
}
