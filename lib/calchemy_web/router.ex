defmodule CalchemyWeb.Router do
  use CalchemyWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {CalchemyWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", CalchemyWeb do
    pipe_through :browser

    get "/", PageController, :home
    post "/new_calendar", PageController, :new_calendar

    live_session :default, on_mount: [CalchemyWeb.CalendarAuth, CalchemyWeb.LiveHook.Misc] do
      live "/week/:id", WeekLive.Index
      live "/week/:id/:year/:month/:day", WeekLive.Index
      live "/month/:id", MonthLive.Index
      live "/month/:id/:year/:month", MonthLive.Index
      live "/past/:id", EventsLive, :past
      live "/future/:id", EventsLive, :future
    end
  end

  # Other scopes may use custom stacks.
  # scope "/api", CalchemyWeb do
  #   pipe_through :api
  # end

  # Enable LiveDashboard and Swoosh mailbox preview in development
  if Application.compile_env(:calchemy, :dev_routes) do
    # If you want to use the LiveDashboard in production, you should put
    # it behind authentication and allow only admins to access it.
    # If your application does not have an admins-only section yet,
    # you can use Plug.BasicAuth to set up some basic authentication
    # as long as you are also using SSL (which you should anyway).
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: CalchemyWeb.Telemetry
      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end
end
