defmodule CalchemyWeb.MonthLive.Index do
  alias Calchemy.DayEvent
  alias Calchemy.DayEvent.Events.{AddEvent, DeleteEvent}
  use CalchemyWeb, :live_view
  alias CalchemyWeb.MonthLive

  def handle_params(params, _url, socket) do
    calendar_id = params["id"]

    if connected?(socket) do
      DayEvent.subscribe(calendar_id)
    end

    {:noreply,
     socket
     |> assign(calendar_id: calendar_id)
     |> apply_action(params)
     |> get_events()}
  end

  defp apply_action(socket, %{"year" => year, "month" => month}) do
    [year, month] = Enum.map([year, month], &String.to_integer/1)
    assign_month(socket, year, month)
  end

  defp apply_action(socket, _params) do
    date = Date.utc_today()
    replace_month(socket, date.year, date.month)
  end

  defp assign_month(socket, year, month) do
    assign(socket, current_year: year, current_month: month)
  end

  defp replace_month(socket, year, month) do
    push_patch(socket, to: ~p"/month/#{socket.assigns.calendar_id}/#{year}/#{month}")
  end

  defp get_events(socket) do
    events =
      if is_nil(Map.get(socket.assigns, :current_year)) do
        %{}
      else
        DayEvent.events_amounts_in_year(socket.assigns.calendar_id, socket.assigns.current_year)
      end

    assign(socket, events: events)
  end

  def render(assigns) do
    ~H'''
    <div class="flex gap-2 my-2 lg:hidden">
      <div>
        <.button id="toggle-year-display" phx-hook="ToggleOneYearView">View Year</.button>
      </div>
      <div class="grow flex gap-2 justify-end">
        <div>
          <.button>
            <.link navigate={~p"/past/#{@calendar_id}"}>
              Past events
            </.link>
          </.button>
        </div>
        <div>
          <.button>
            <.link navigate={~p"/future/#{@calendar_id}"}>
              Future events
            </.link>
          </.button>
        </div>
      </div>
    </div>
    <div class="flex">
      <div
        id="one-year-view"
        class="bg-slate-900 z-10 hidden w-full absolute lg:w-2/12 lg:block lg:relative"
      >
        <div>
          <.view_year year={@current_year - 1} month={@current_month} calendar_id={@calendar_id} />
          <.view_year year={@current_year + 1} month={@current_month} calendar_id={@calendar_id} />
        </div>
        <.live_component
          :for={month <- 1..12}
          module={MonthLive.ViewMonth}
          id={"small_month_#{month}"}
          year={@current_year}
          month={month}
          calendar_id={@calendar_id}
          events={Map.get(@events, month, %{})}
        />
      </div>
      <div class="w-1/12 hidden lg:block"></div>
      <div class="w-full lg:w-8/12">
        <div class="fixed w-full lg:w-8/12">
          <div class="justify-end gap-2 my-2 hidden lg:flex">
            <div>
              <.button>
                <.link navigate={~p"/past/#{@calendar_id}"}>
                  Past events
                </.link>
              </.button>
            </div>
            <div>
              <.button>
                <.link navigate={~p"/future/#{@calendar_id}"}>
                  Future events
                </.link>
              </.button>
            </div>
          </div>
          <.live_component
            module={MonthLive.ViewMonth}
            id="main_month"
            year={@current_year}
            month={@current_month}
            calendar_id={@calendar_id}
            is_large={true}
            events={Map.get(@events, @current_month, %{})}
          />
        </div>
      </div>
      <div class="w-1/12 hidden lg:block"></div>
    </div>
    '''
  end

  attr :year, :integer, required: true
  attr :month, :integer, required: true
  attr :calendar_id, :string, required: true

  defp view_year(assigns) do
    ~H'''
    <div class="text-center my-2">
      <.link
        navigate={~p"/month/#{@calendar_id}/#{@year}/#{@month}"}
        class="bg-slate-700 hover:bg-slate-600 block py-1"
      >
        View year <%= @year %>
      </.link>
    </div>
    '''
  end

  def handle_info({DayEvent, %AddEvent{} = add_ev}, socket) do
    {:noreply, update_event(socket, add_ev.event.date, 1)}
  end

  def handle_info({DayEvent, %DeleteEvent{} = del_ev}, socket) do
    {:noreply, update_event(socket, del_ev.event.date, -1)}
  end

  def handle_info({DayEvent, _}, socket) do
    {:noreply, socket}
  end

  def update_event(socket, ev_date, shift) when ev_date.year == socket.assigns.current_year do
    day_events = %{ev_date.day => max(0, shift)}

    events =
      Map.update(socket.assigns.events, ev_date.month, day_events, fn month_events ->
        Map.update(month_events, ev_date.day, day_events, &max(0, &1 + shift))
      end)

    assign(socket, events: events)
  end

  def update_event(socket, _, _), do: socket
end
