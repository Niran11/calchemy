defmodule CalchemyWeb.MonthLive.ViewMonth do
  use CalchemyWeb, :live_component

  def update(new_assigns, socket) do
    socket = assign(socket, new_assigns)

    socket =
      if Map.get(socket.assigns, :initial_load, true) do
        begin_date = Date.new!(socket.assigns.year, socket.assigns.month, 1)
        end_date = Date.end_of_month(begin_date)
        days = Date.range(begin_date, end_date)

        assign(socket,
          initial_load: false,
          fill_start: Date.day_of_week(begin_date) - 1,
          fill_end: 7 - Date.day_of_week(end_date),
          first_day: begin_date,
          days: days
        )
      else
        socket
      end

    {:ok, socket}
  end

  attr :year, :integer, required: true
  attr :month, :integer, required: true
  attr :calendar_id, :string, required: true
  attr :is_large, :boolean, default: false
  attr :events, :map, required: true

  def render(assigns) do
    ~H'''
    <div>
      <%= if @is_large do %>
        <.view_month
          first_day={@first_day}
          fill_start={@fill_start}
          fill_end={@fill_end}
          days={@days}
          events={@events}
          calendar_id={@calendar_id}
          is_large={@is_large}
        />
      <% else %>
        <.link navigate={~p"/month/#{@calendar_id}/#{@year}/#{@month}"} class="block">
          <.view_month
            first_day={@first_day}
            fill_start={@fill_start}
            fill_end={@fill_end}
            days={@days}
            events={@events}
            calendar_id={@calendar_id}
            is_large={@is_large}
          />
        </.link>
      <% end %>
    </div>
    '''
  end

  def view_month(assigns) do
    ~H'''
    <div class={["text-center py-2", if(@is_large, do: "text-3xl", else: "text-xl")]}>
      <%= Calendar.strftime(@first_day, "%B %Y") %>
    </div>
    <div class={["grid grid-cols-7 auto-rows-fr gap-1 lg:gap-2 month", @is_large && "is-large"]}>
      <div :for={_ <- 1..@fill_start}></div>
      <.view_day
        :for={day <- @days}
        day={day}
        events_amount={Map.get(@events, day.day, 0)}
        calendar_id={@calendar_id}
        is_large={@is_large}
      />
      <div :for={_ <- 1..@fill_end}></div>
    </div>
    '''
  end

  attr :day, :any, required: true
  attr :events_amount, :integer, required: true
  attr :calendar_id, :string, required: true
  attr :is_large, :boolean, default: false

  def view_day(assigns) do
    ~H'''
    <div>
      <%= if @is_large do %>
        <.link navigate={~p"/week/#{@calendar_id}/#{@day.year}/#{@day.month}/#{@day.day}"}>
          <.display_day_info day={@day} events_amount={@events_amount} is_large={@is_large} />
        </.link>
      <% else %>
        <.display_day_info day={@day} events_amount={@events_amount} is_large={@is_large} />
      <% end %>
    </div>
    '''
  end

  defp display_day_info(assigns) do
    ~H'''
    <div class={["day", @events_amount > 0 && "has-events"]}>
      <div class="text-center">
        <%= Calendar.strftime(@day, "%d") %>
        <div class="text-xs">
          <%= if @is_large and @events_amount > 0 do %>
            Ev<span class="hidden lg:inline">ent</span>s: <b><%= @events_amount %></b>
          <% end %>
        </div>
      </div>
    </div>
    '''
  end
end
