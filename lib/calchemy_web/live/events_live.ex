defmodule CalchemyWeb.EventsLive do
  alias Calchemy.DayEvent
  alias Calchemy.DayEvent.Events.{AddEvent, DeleteEvent, UpdateEvent}
  use CalchemyWeb, :live_view
  alias CalchemyWeb.LiveComponent.CalendarEvent

  def mount(params, _, socket) do
    calendar_id = params["id"]

    if connected?(socket) do
      DayEvent.subscribe(calendar_id)
    end

    {:ok,
     socket
     |> assign(
       edited_event: nil,
       calendar_id: calendar_id
     )
     |> apply_action()
     |> load_events(Date.utc_today())}
  end

  defp apply_action(socket) when socket.assigns.live_action == :past do
    calendar_id = socket.assigns.calendar_id

    assign(socket,
      get_events_cb: &DayEvent.get_before(calendar_id, &1),
      page_title: "Past events",
      add_filter_cb: &Date.before?/2,
      events_sort_order: :desc
    )
  end

  defp apply_action(socket) when socket.assigns.live_action == :future do
    calendar_id = socket.assigns.calendar_id

    assign(socket,
      get_events_cb: &DayEvent.get_on_or_after(calendar_id, &1),
      page_title: "Current events",
      add_filter_cb: &(Date.compare(&1, &2) in [:eq, :gt]),
      events_sort_order: :asc
    )
  end

  def render(assigns) do
    ~H'''
    <div class="m-2 grid grid-cols-12 items-center">
      <div class="col-span-4">
        <.button>
          <.link navigate={~p"/month/#{@calendar_id}"} class="px-1">
            View Month
          </.link>
        </.button>
      </div>
      <div class="text-center text-xl col-span-5">
        <%= @page_title %>
      </div>
    </div>
    <div
      phx-viewport-bottom={@last_day && "load_more_events"}
      id="events_container"
      class="px-0.5 md:px-2"
    >
      <div class="hidden only:block text-center">No events</div>
      <div :for={event <- @events} class="flex bg-slate-500 my-2">
        <div class="self-stretch flex items-center border-r-2 border-slate-800 px-2">
          <.link navigate={
            ~p"/week/#{@calendar_id}/#{event.date.year}/#{event.date.month}/#{event.date.day}"
          }>
            <%= Calendar.strftime(event.date, "%d.%m.%Y") %>
          </.link>
        </div>
        <div class="grow">
          <%= if event.id == @edited_event do %>
            <.live_component module={CalendarEvent.Edit} id={"edit_event-#{event.id}"} event={event} />
          <% else %>
            <.live_component
              module={CalendarEvent.View}
              id={"view_event-#{event.id}"}
              event={event}
              calendar_id={@calendar_id}
            />
          <% end %>
        </div>
      </div>
    </div>
    '''
  end

  def handle_event("load_more_events", _, socket) when is_nil(socket.assigns.last_day) do
    {:noreply, socket}
  end

  def handle_event("load_more_events", _, socket) do
    {:noreply, load_events(socket, socket.assigns.last_day)}
  end

  defp load_events(socket, nil), do: socket

  defp load_events(socket, date) do
    events = socket.assigns.get_events_cb.(date)
    last_ev = List.last(events)
    last_day = last_ev && last_ev.date
    assign(socket, last_day: last_day, events: Map.get(socket.assigns, :events, []) ++ events)
  end

  def handle_info({DayEvent, %AddEvent{} = add_ev}, socket) do
    if socket.assigns.add_filter_cb.(add_ev.event.date, Date.utc_today()) do
      events =
        [add_ev.event | socket.assigns.events]
        |> Enum.sort_by(& &1.date, socket.assigns.events_sort_order)

      {:noreply, assign(socket, events: events)}
    else
      {:noreply, socket}
    end
  end

  def handle_info({DayEvent, %DeleteEvent{} = del_ev}, socket) do
    events = Enum.reject(socket.assigns.events, &(&1.id == del_ev.event.id))
    {:noreply, assign(socket, events: events)}
  end

  def handle_info({DayEvent, %UpdateEvent{} = up_ev}, socket) do
    event = up_ev.event

    events =
      Enum.map(socket.assigns.events, fn
        ev when ev.id == event.id -> %{ev | text: event.text}
        ev -> ev
      end)

    {:noreply, assign(socket, events: events)}
  end

  def handle_info(%CalendarEvent.Events.ChangeEditing{} = ched_ev, socket) do
    {:noreply, assign(socket, edited_event: ched_ev.event_id)}
  end
end
