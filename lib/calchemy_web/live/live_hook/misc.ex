defmodule CalchemyWeb.LiveHook.Misc do
  use CalchemyWeb, :live_view

  def on_mount(:default, _params, _session, socket) do
    {:cont, attach_hook(socket, :put_flash, :handle_info, &handle_info/2)}
  end

  def handle_info({:put_flash, kind, msg}, socket) do
    {:halt, put_flash(socket, kind, msg)}
  end

  def handle_info(_msg, socket), do: {:cont, socket}
end
