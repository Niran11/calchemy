defmodule CalchemyWeb.WeekLive.ChangeWeek do
  alias CalchemyWeb.WeekLive
  import CalchemyWeb.CoreComponents
  use CalchemyWeb, :live_component

  def render(assigns) do
    ~H'''
    <div class="grid lg:grid-cols-2 gap-2">
      <.button phx-click="previous_week" phx-target={@myself}>
        <%= "<" %> Previous
      </.button>
      <.button phx-click="next_week" phx-target={@myself}>
        Next <%= ">" %>
      </.button>
    </div>
    '''
  end

  def handle_event("previous_week", _, socket), do: change_week(socket, -1)

  def handle_event("next_week", _, socket), do: change_week(socket, 1)

  defp change_week(socket, shift) do
    send(self(), %WeekLive.Events.ChangeWeek{direction: shift})
    {:noreply, socket}
  end
end
