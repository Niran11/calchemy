defmodule CalchemyWeb.WeekLive.Events do
  defmodule ChangeWeek do
    defstruct direction: nil
  end
end
