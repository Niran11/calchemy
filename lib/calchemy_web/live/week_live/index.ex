defmodule CalchemyWeb.WeekLive.Index do
  alias Calchemy.DayEvent
  alias Calchemy.DayEvent.Events.{AddEvent, DeleteEvent, UpdateEvent}
  use CalchemyWeb, :live_view
  alias CalchemyWeb.WeekLive
  alias CalchemyWeb.LiveComponent.CalendarEvent

  def mount(params, _session, socket) do
    calendar_id = params["id"]

    if connected?(socket) do
      DayEvent.subscribe(calendar_id)
    end

    {:ok, assign(socket, edited_event: nil, calendar_id: calendar_id)}
  end

  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, params)}
  end

  defp apply_action(socket, %{"year" => year, "month" => month, "day" => day}) do
    [year, month, day] = Enum.map([year, month, day], &String.to_integer/1)
    assign_dates(socket, Date.new!(year, month, day))
  end

  defp apply_action(socket, _params) do
    date = Date.utc_today()
    replace_week(socket, date)
  end

  defp assign_dates(socket, main_date) do
    begin_date = Date.beginning_of_week(main_date)
    end_date = Date.end_of_week(main_date)
    dates = Date.range(begin_date, end_date)

    events =
      socket.assigns.calendar_id
      |> DayEvent.get_in_week(main_date)
      |> Enum.group_by(& &1.date)

    assign(socket, dates: dates, main_date: main_date, events: events)
  end

  def render(assigns) do
    ~H'''
    <div class="flex items-center my-2">
      <div><.live_component module={WeekLive.ChangeWeek} id="change_week" /></div>
      <div class="grow flex justify-end gap-2 text-center">
        <div>
          <.button>
            <.link navigate={~p"/past/#{@calendar_id}"}>
              Past events
            </.link>
          </.button>
        </div>
        <div>
          <.button>
            <.link navigate={~p"/future/#{@calendar_id}"}>
              Future events
            </.link>
          </.button>
        </div>
        <div>
          <.button>
            <.link navigate={~p"/month/#{@calendar_id}/#{@main_date.year}/#{@main_date.month}"}>
              Month view
            </.link>
          </.button>
        </div>
      </div>
    </div>
    <div class="flex gap-2 text-center flex-wrap lg:flex-nowrap">
      <%= for day <- @dates do %>
        <.display_day
          main_date={@main_date}
          day={day}
          events={Map.get(@events, day, [])}
          calendar_id={@calendar_id}
          edited_event={@edited_event}
        />
      <% end %>
    </div>
    '''
  end

  attr :main_date, :any, required: true
  attr :day, :any, required: true
  attr :events, :any, required: true
  attr :calendar_id, :string, required: true
  attr :edited_event, :string, required: true
  attr :rest, :global

  defp display_day(assigns) do
    ~H'''
    <div
      class="bg-slate-600 w-full xl:w-1/5 h-min"
      id={"day-#{@day}"}
      phx-hook={@main_date == @day && "ScrollToMainDate"}
    >
      <.day_date day={@day} />
      <div :for={event <- @events} class="text-left">
        <%= if event.id == @edited_event do %>
          <.live_component module={CalendarEvent.Edit} id={"event-#{event.id}"} event={event} />
        <% else %>
          <.live_component
            module={CalendarEvent.View}
            class="border-y-2 border-slate-800"
            id={"event-#{event.id}"}
            event={event}
            calendar_id={@calendar_id}
          />
        <% end %>
      </div>
      <.live_component
        module={WeekLive.NewEvent}
        id={"day_new_event-#{@day}"}
        day={@day}
        calendar_id={@calendar_id}
      />
    </div>
    '''
  end

  attr :day, :any, required: true

  defp day_date(assigns) do
    ~H'''
    <div class="text-xl border-b-2 bg-slate-600 border-slate-800">
      <%= Calendar.strftime(@day, "%d.%m.%Y") %>
    </div>
    '''
  end

  def handle_info({DayEvent, %AddEvent{} = add_ev}, socket) do
    event = add_ev.event
    {:noreply, update_event(socket, event, &(&1 ++ [event]))}
  end

  def handle_info({DayEvent, %DeleteEvent{} = del_ev}, socket) do
    event = del_ev.event

    {:noreply, update_event(socket, event, fn val -> Enum.reject(val, &(&1.id == event.id)) end)}
  end

  def handle_info({DayEvent, %UpdateEvent{} = up_ev}, socket) do
    event = up_ev.event

    {:noreply,
     update_event(socket, event, fn val ->
       index = Enum.find_index(val, &(&1.id == event.id))
       List.update_at(val, index, &%{&1 | text: event.text})
     end)}
  end

  def handle_info(%CalendarEvent.Events.ChangeEditing{} = ched_ev, socket) do
    event_id = ched_ev.event_id
    {:noreply, assign(socket, edited_event: event_id)}
  end

  def handle_info(%WeekLive.Events.ChangeWeek{} = chw_ev, socket) do
    shift = chw_ev.direction * 7
    date = socket.assigns.dates |> Enum.at(0) |> Date.add(shift)
    {:noreply, replace_week(socket, date)}
  end

  def handle_info(_, socket) do
    {:noreply, socket}
  end

  defp update_event(socket, event, update_val) do
    if Enum.member?(socket.assigns.dates, event.date) do
      events =
        socket.assigns.events
        |> Map.put_new(event.date, [])
        |> Map.update!(event.date, update_val)

      assign(socket, events: events)
    else
      socket
    end
  end

  defp replace_week(socket, date) do
    push_patch(socket,
      to: ~p"/week/#{socket.assigns.calendar_id}/#{date.year}/#{date.month}/#{date.day}"
    )
  end
end
