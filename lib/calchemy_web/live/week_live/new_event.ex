defmodule CalchemyWeb.WeekLive.NewEvent do
  alias Calchemy.DayEvent
  use CalchemyWeb, :live_component
  alias CalchemyWeb.LiveComponent.CalendarEvent

  attr :day, :any, required: true
  attr :calendar_id, :string, required: true

  def render(assigns) do
    ~H'''
    <div class="border-t-2 border-slate-800">
      <.button class="text-xl w-full" phx-click="new_event" value={@day} phx-target={@myself}>
        +
      </.button>
    </div>
    '''
  end

  def handle_event("new_event", %{"value" => day}, socket) do
    calendar_id = socket.assigns.calendar_id

    %DayEvent{
      calendar_id: calendar_id,
      date: Date.from_iso8601!(day),
      text: ""
    }
    |> DayEvent.add_event()
    |> process_event_add(socket)
  end

  def process_event_add({:ok, event_id}, socket) do
    send(self(), %CalendarEvent.Events.ChangeEditing{event_id: event_id})
    {:noreply, socket}
  end

  def process_event_add({:error, _}, socket) do
    put_flash!(:error, "Could not create new event!")
    {:noreply, socket}
  end
end
