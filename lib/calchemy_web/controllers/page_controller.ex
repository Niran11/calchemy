defmodule CalchemyWeb.PageController do
  use CalchemyWeb, :controller
  alias Calchemy.Calendar

  def home(conn, _params) do
    # The home page is often custom made,
    # so skip the default app layout.
    render(conn, :home, layout: false)
  end

  def new_calendar(conn, _params) do
    case(Calendar.create_new_calendar()) do
      {:ok, id} ->
        redirect(conn, to: ~p"/month/#{id}")

      {:error, _} ->
        render(conn |> put_flash(:error, "Cannot create new calendar"), :home, layout: false)
    end
  end
end
