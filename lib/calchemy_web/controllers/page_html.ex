defmodule CalchemyWeb.PageHTML do
  use CalchemyWeb, :html

  embed_templates "page_html/*"
end
