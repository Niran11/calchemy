defmodule CalchemyWeb.LiveComponent.CalendarEvent.View do
  alias CalchemyWeb.LiveComponent.CalendarEvent
  use CalchemyWeb, :live_component

  attr :id, :any, required: true
  attr :class, :any, default: nil
  attr :event, :any, required: true
  attr :calendar_id, :string, required: true

  def render(assigns) do
    ~H'''
    <div class={["bg-slate-500 p-1", @class]}>
      <div phx-click="start_editing" phx-target={@myself}>
        <.live_component
          module={CalendarEvent.Delete}
          id={"#{@id}-delete_event"}
          event_id={@event.id}
          date={@event.date}
          calendar_id={@calendar_id}
        />
        <div class="whitespace-pre-wrap" style="min-height:1.5rem"><%= @event.text %></div>
      </div>
    </div>
    '''
  end

  def handle_event("start_editing", _, socket) do
    send(self(), %CalendarEvent.Events.ChangeEditing{event_id: socket.assigns.event.id})
    {:noreply, socket}
  end
end
