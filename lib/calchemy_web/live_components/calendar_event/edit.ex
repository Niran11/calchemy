defmodule CalchemyWeb.LiveComponent.CalendarEvent.Edit do
  alias Calchemy.DayEvent
  use CalchemyWeb, :live_component
  alias CalchemyWeb.LiveComponent.CalendarEvent

  attr :event, :any, required: true

  def render(assigns) do
    ~H'''
    <div class="bg-slate-500 border-y-2 border-slate-800">
      <textarea
        id={"#{@id}-textarea"}
        class="p-1 mx-0 w-full !border-0 !bg-inherit resize-y"
        name="text"
        phx-hook="TextareaFocusHook"
        phx-blur="event_saved"
        phx-target={@myself}
      ><%= @event.text %></textarea>
    </div>
    '''
  end

  def handle_event("event_saved", %{"value" => event_text}, socket) do
    event = socket.assigns.event
    update_result = DayEvent.update_event(event.calendar_id, event.id, event_text)
    {:noreply, handle_update_result(update_result, socket)}
  end

  defp handle_update_result(:ok, socket) do
    send(self(), %CalendarEvent.Events.ChangeEditing{event_id: nil})
    socket
  end

  defp handle_update_result(:error, socket) do
    put_flash!(:error, "Could not save updated event")
    {:noreply, socket}
  end
end
