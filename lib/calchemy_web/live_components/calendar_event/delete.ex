defmodule CalchemyWeb.LiveComponent.CalendarEvent.Delete do
  alias Calchemy.DayEvent
  use CalchemyWeb, :live_component

  def mount(socket) do
    {:ok, assign(socket, deleting: false)}
  end

  attr :event_id, :any, required: true
  attr :date, :any, required: true
  attr :delete_event, :string, required: true

  def render(assigns) do
    ~H'''
    <div class="text-right">
      <%= if @deleting do %>
        <.button
          class="!bg-slate-700 hover:!bg-green-800 mr-0"
          title="delete event"
          phx-click="confirm_delete"
          phx-target={@myself}
        >
          V
        </.button>
        <.button
          class="!bg-slate-700 hover:!bg-red-800 ml-0"
          title="cancel"
          phx-click="cancel_delete"
          phx-target={@myself}
        >
          X
        </.button>
      <% else %>
        <.button class="!bg-slate-700 hover:!bg-red-800" phx-click="delete_event" phx-target={@myself}>
          delete
        </.button>
      <% end %>
    </div>
    '''
  end

  def handle_event("delete_event", _, socket) do
    {:noreply, assign(socket, deleting: true)}
  end

  def handle_event("cancel_delete", _, socket) do
    {:ok, socket} = mount(socket)
    {:noreply, socket}
  end

  def handle_event("confirm_delete", _, socket) do
    socket.assigns.calendar_id
    |> DayEvent.remove_event(socket.assigns.event_id)
    |> after_event_remove(socket)
  end

  def after_event_remove(:ok, socket) do
    put_flash!(:info, "Event deleted!")
    {:ok, socket} = mount(socket)
    {:noreply, socket}
  end

  def after_event_remove(:error, socket) do
    put_flash!(:error, "Failed to delete event!")
    {:noreply, socket}
  end
end
