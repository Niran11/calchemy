defmodule CalchemyWeb.Layouts do
  use CalchemyWeb, :html

  embed_templates "layouts/*"
end
