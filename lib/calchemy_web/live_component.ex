defmodule CalchemyWeb.LiveComponent do
  def put_flash!(kind, msg) do
    send(self(), {:put_flash, kind, msg})
  end
end
