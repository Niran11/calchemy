defmodule CalchemyWeb.CalendarAuth do
  alias Calchemy.Calendar
  import Phoenix.LiveView
  use CalchemyWeb, :verified_routes

  def on_mount(:default, %{"id" => calendar_id}, _session, socket) do
    if Calendar.exists?(calendar_id) do
      {:cont, socket}
    else
      {:halt, socket |> put_flash(:error, "That calendar does not exist!") |> redirect(to: ~p"/")}
    end
  end
end
