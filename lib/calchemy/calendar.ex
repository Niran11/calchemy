defmodule Calchemy.Calendar do
  use Ecto.Schema
  alias Calchemy.{Repo, Calendar}
  import Ecto.Query

  @primary_key {:id, Ecto.UUID, autogenerate: true}
  schema "calendars" do
    timestamps()
  end

  def create_new_calendar() do
    {:ok, calendar} = %Calchemy.Calendar{} |> Repo.insert()
    {:ok, calendar.id}
  end

  def exists?(calendar_id) do
    case Ecto.UUID.cast(calendar_id) do
      {:ok, calendar_id} -> Repo.exists?(from cal in Calendar, where: cal.id == ^calendar_id)
      :error -> false
    end
  end
end
