defmodule Calchemy.DayEvent do
  alias Calchemy.{Repo, DayEvent.Events}
  use Ecto.Schema
  import Ecto.{Changeset, Query}

  @pubsub Calchemy.PubSub

  @primary_key {:id, Ecto.UUID, autogenerate: true}
  schema "day_events" do
    field :calendar_id, Ecto.UUID
    field :date, :date
    field :text, :string, default: ""

    timestamps()
  end

  def get_before(calendar_id, date) do
    sub =
      calendar_id
      |> base_query(:desc)
      |> where([ev], ev.date < ^date)
      |> limit(20)
      |> select([ev], ev.date)

    calendar_id
    |> base_query(:desc)
    |> where([ev], ev.date in subquery(sub))
    |> Repo.all()
  end

  def get_on_or_after(calendar_id, date) do
    sub =
      calendar_id
      |> base_query()
      |> where([ev], ev.date >= ^date)
      |> limit(20)
      |> select([ev], ev.date)

    calendar_id
    |> base_query()
    |> where([ev], ev.date in subquery(sub))
    |> Repo.all()
  end

  def get_in_week(calendar_id, %Date{} = date) do
    week_begin = Date.beginning_of_week(date)
    week_end = Date.end_of_week(date)

    calendar_id
    |> base_query()
    |> where([ev], ev.date >= ^week_begin and ev.date <= ^week_end)
    |> Repo.all()
  end

  defp base_query(calendar_id, order_direction \\ :asc) do
    order = [{order_direction, :date}, {order_direction, :inserted_at}]

    from(ev in __MODULE__,
      where: ev.calendar_id == ^calendar_id,
      order_by: ^order
    )
  end

  def events_amounts_in_year(calendar_id, year) do
    from(ev in __MODULE__,
      where:
        ev.calendar_id == ^calendar_id and
          fragment("DATE_PART('year', ?)", ev.date) == ^year,
      group_by: ev.date,
      select: {ev.date, count(ev.id)}
    )
    |> Repo.all()
    |> Enum.map(fn {date, amount} -> {date.month, date.day, amount} end)
    |> Enum.reduce(%{}, fn {month, day, amount}, acc ->
      Map.update(acc, month, %{day => amount}, &Map.put(&1, day, amount))
    end)
  end

  def add_event(%__MODULE__{} = event) do
    event |> changeset(%{}) |> Repo.insert(returning: false) |> broadcast_added_event()
  end

  defp broadcast_added_event({:ok, event}) do
    broadcast!(event.calendar_id, %Events.AddEvent{event: event})
    {:ok, event.id}
  end

  defp broadcast_added_event({:error, event}), do: {:error, event}

  def update_event(calendar_id, event_id, text) do
    Repo.one(from ev in __MODULE__, where: ev.calendar_id == ^calendar_id and ev.id == ^event_id)
    |> update_event(text)
    |> broadcast_update(calendar_id)
  end

  defp update_event(nil, _text) do
    {:error, :error}
  end

  defp update_event(event, text) do
    event |> changeset(%{text: text}) |> Repo.update()
  end

  defp broadcast_update({:ok, event}, calendar_id) do
    broadcast!(calendar_id, %Events.UpdateEvent{event: event})
    :ok
  end

  defp broadcast_update({:error, _}, _calendar_id), do: :error

  def remove_event(calendar_id, event_id) do
    Repo.one(from ev in __MODULE__, where: ev.calendar_id == ^calendar_id and ev.id == ^event_id)
    |> delete_event()
    |> broadcast_deletion(calendar_id)
  end

  defp delete_event(nil) do
    {:error, :error}
  end

  defp delete_event(event) do
    Repo.delete(event)
  end

  defp broadcast_deletion({:ok, event}, calendar_id) do
    broadcast!(calendar_id, %Events.DeleteEvent{event: event})
    :ok
  end

  defp broadcast_deletion({:error, _}, _calendar_id), do: :error

  @doc false
  defp changeset(day_event, attrs) do
    day_event
    |> cast(attrs, [:calendar_id, :text, :date])
    |> validate_required([:calendar_id, :date])
    |> foreign_key_constraint(:calendar_id)
  end

  def subscribe(calendar_id), do: Phoenix.PubSub.subscribe(@pubsub, topic(calendar_id))

  defp topic(calendar_id), do: "calendar:" <> calendar_id

  defp broadcast!(calendar_id, msg) do
    Phoenix.PubSub.broadcast!(@pubsub, topic(calendar_id), {__MODULE__, msg})
  end
end
