defmodule Calchemy.DayEvent.Events do
  defmodule AddEvent do
    defstruct event: nil
  end

  defmodule DeleteEvent do
    defstruct event: nil
  end

  defmodule UpdateEvent do
    defstruct event: nil
  end
end
