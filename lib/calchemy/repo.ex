defmodule Calchemy.Repo do
  use Ecto.Repo,
    otp_app: :calchemy,
    adapter: Ecto.Adapters.Postgres
end
