# Calchemy
This is simple calendar.
You can generate new calendar and store events in it.
As long as you have url to it, you can share it with your friends and family.
You all can add/modify/delete events in that calendar together in real time.
No login required, just one simple url.

## production deployment
```sh
mix deps.get --only prod # download dependencies
MIX_ENV=prod mix compile # compile app
MIX_ENV=prod mix assets.deploy # build assets

mix phx.gen.secret # generate secret
export SECRET_KEY_BASE='secret' # from previous line
export DATABASE_URL=ecto://USER:PASS@HOST/database
MIX_ENV=prod mix ecto.setup # setup db and run migrations
PORT=4001 MIX_ENV=prod mix phx.server # run server
```

# development deployment
```sh
mix deps.get
mix compile
mix ecto.setup
mix phx.server
```